const guardarCliente = async (req, res) => {
    const {cedula, nombres, apellidos, direccion, correo, telefono} = req.body

    const expresiones = {
        nombres: /^([a-zA-Z]{4,})\s([a-zA-Z]+)*$/, 
        cedula: /^\d{10}$/,
        direccion: /^[A-Za-z]{,40}/,
        correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
        telefono:/^\d{10}$/,
    }

    if(!expresiones.nombres.test(nombre)){
        res.json({'error': 'Ingrese los nombres completos'})
    }else  if(!expresiones.nombres.test(apellido)){
        res.json({'error': 'Ingrese los apellidos completos'})
    }else if(!expresiones.cedula.test(cedula)){
        res.json({'error': 'La cedula debe tener 10 digitos'})
    }else if(!expresiones.direccion.test(direccion)){
        res.json({'error': 'La dirección debe tener maximo 40'})
    }else if(!expresiones.correo.test(correo)){
        res.json({'error': 'Correo electronico no válido (example@something.com)'})
    }else if(!expresiones.telefono.test(telefono)){
        res.json({'error': 'El telefono debe tener 10 digitos'})
    }else{
        try {
            await pool.query(
                "INSERT INTO cliente(cedula, nombres, apellidos,  direccion, correo, telefono) VALUES($1, $2, $3, $4, $5, $6) RETURNING *",
                [cedula, nombres, apellidos, direccion , correo ,telefono],
            (error, result) =>{
                    if(error){
                        res.json({'error': error})
                    }else{
                        res.json(result.rows)
                    }
                }
            );
        } catch (error) {
            res.json({'error': error})
        }
    }
    
}
